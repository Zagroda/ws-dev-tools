/* jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */
/* global $:false, window:false, jdetects:false */

var t2 = (function () {
  'use strict';
  var $t = {};
  
  jdetects.create(function(status) {
    $('#t2').data('tools', status);
  });
  
  var i = window.setInterval(function(){
    var $t = $('#t2');
    var val = $('#people td:contains("Justin")').length;
    var condition1 = (val <= 0) ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
    }
  }, 1000);
  
  return $t;
}());