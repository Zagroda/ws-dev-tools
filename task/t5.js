/* jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */
/* global $:false, window:false, jdetects:false */

var t4 = (function () {
  'use strict';
  var $t = {};

  jdetects.create(function(status) {
    $('#t5').data('tools', status);
  });

  var i = window.setInterval(function(){
    var $t = $('#t5');
    var val = $('#people td:contains("Angelina Jolie")').parent().hasClass('highlight');
    console.info(val);
    var condition1 = (val === true) ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
    }
  }, 1000);

  return $t;
}());