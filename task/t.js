/* jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */
/* global $:false, window:false */

var t1 = (function () {
  'use strict';
  var $t = {};
  
  $('.hide').hide();
  
  $('#people').on('contextmenu', function(e){
    e.stopPropagation();
    $('#t1').data('value', e.target.innerText);
  });
  
  var i = window.setInterval(function(){
    var $t = $('#t1');
    var val = $t.data('value');
    var condition1 = (val === 'Justin Bieber' || val === 'justin.bieber@performgroup.com' || val === '+48 666-666-666') ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t2').removeClass('hide').show(1000);
    }
  }, 2000);
  
  return $t;
}());

var t2 = (function () {
  'use strict';
  var $t = {};

  var i = window.setInterval(function(){
    var $t = $('#t2');
    var val = $('#people td:contains("Justin")').length;
    var condition1 = (val <= 0) ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t3').removeClass('hide').show(1000);
    }
  }, 2000);

  return $t;
}());

var t3 = (function () {
  'use strict';
  var $t = {};

  var i = window.setInterval(function(){
    var $t = $('#t3');
    var val = $('#people td:contains("+48 756-284-219")').length;
    var condition1 = (val >= 1) ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t4').removeClass('hide').show(1000);
    }
  }, 2000);

  return $t;
}());

var t4 = (function () {
  'use strict';
  var $t = {};

  var i = window.setInterval(function(){
    var $t = $('#t4');
    var val = $('#people tr:eq(1) td:eq(0)').text();
    var condition1 = (val === 'Angelina Jolie') ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t5').removeClass('hide').show(1000);
    }
  }, 2000);

  return $t;
}());

var t5 = (function () {
  'use strict';
  var $t = {};

  var i = window.setInterval(function(){
    var $t = $('#t5');
    var val = $('#people td:contains("Angelina Jolie")').parent().hasClass('highlight');
    var condition1 = (val === true) ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t6').removeClass('hide').show(1000);
    }
  }, 2000);

  return $t;
}());

var t6 = (function () {
  'use strict';
  var $t = {};

  var i = window.setInterval(function(){
    var $t = $('#t6');
    var val = $('#people').css('font-family');
    var condition1 = (val.indexOf('Helvetica') < 0) ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t7').removeClass('hide').show(1000);
    }
  }, 2000);

  return $t;
}());

var t7 = (function () {
  'use strict';
  var $t = {};

  var i = window.setInterval(function(){
    var $t = $('#t7');
    var val = $('.highlight').css('background-color');
    var condition1 = (val === 'rgb(124, 252, 0)') ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t8').removeClass('hide').show(1000);
    }
  }, 2000);

  return $t;
}());

var t8 = (function () {
  'use strict';
  var $t = {};
  
//  function checkTask() {
//    var linkTag = $('link[href*="styles.css"]');
//    var linkTagHref = linkTag.attr('href').split('?');
//    var timeStamp = new Date().getTime();
//    linkTag.attr('href', linkTagHref[0] + '?t=' + (timeStamp / 1000));
//  }
//  
//  $('#t8 .check').click(function(){
//    checkTask();
//  });

  var i = window.setInterval(function(){
    var $t = $('#t8');
    var val = $('#people th').css('font-style');
    var condition1 = (val === 'normal') ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t9').removeClass('hide').show(1000).data('value', 'changed');
    }
  }, 2000);

  return $t;
}());

var t9 = (function () {
  'use strict';
  var $t = {};

  var i = window.setInterval(function(){
    var $t = $('#t9');
    var val = $('#people th').css('font-style');
    var condition1 = (val === 'italic') ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    var condition3 = ($t.data('value') === 'changed') ? true:false;
    console.info(condition1, condition2, condition3);
    if (condition1 && condition2 && condition3) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
      $t.hide(1000);
      $('#t10').removeClass('hide').show(1000);
      window.alert('The game is over!');
    }
  }, 2000);

  return $t;
}());