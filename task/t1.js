/* jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */
/* global $:false, window:false, jdetects:false */

var t1 = (function () {
  'use strict';
  var $t = {};
  
  $('#people').on('contextmenu', function(e){
    e.stopPropagation();
    $('#t1').data('value', e.target.innerText);
  });
  
  jdetects.create(function(status) {
    $('#t1').data('tools', status);
  });
  
  var i = window.setInterval(function(){
    var $t = $('#t1');
    var val = $t.data('value');
    var condition1 = (val === 'Justin Bieber' || val === 'justin.bieber@performgroup.com' || val === '+48 666-666-666') ? true:false;
    var condition2 = ($t.data('tools') === 'on') ? true:false;
    if (condition1 && condition2) {
      $t.removeClass('red').addClass('green');
      window.clearInterval(i);
    }
  }, 1000);
  
  return $t;
}());